import React from 'react';
import API from './API';

const Mobile = (props) => {

    const [data, setData] = React.useState({});
    const [dateTime, setDateTime] = React.useState('');
    const [sunrise, setSunrise] = React.useState('');
    const [sunset, setSunset] = React.useState('');

    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    React.useEffect(() => {
        API.get(`/weather`).then((res) => {
            setData(res.data);
            let date = new Date(res.data.timestamp * 1000);
            let day = date.getDay();
            let month = monthNames[date.getMonth()];
            let hours = date.getHours();
            let minutes = "0" + date.getMinutes();

            let formattedTime = hours + ':' + minutes.substr(-2) + ' ' + month + ' ' + day;
            setDateTime(formattedTime);

            date = new Date(res.data.sunrise * 1000);
            let sunrise = date.getHours() + ':' + date.getMinutes();
            setSunrise(sunrise);

            date = new Date(res.data.sunset * 1000);
            let sunset = date.getHours() + ':' + date.getMinutes();
            setSunset(sunset);
        }).catch((err) => {
            console.log(err);
        });
    }, []);

    return (
        <div className='d-block d-lg-none'>
            <div className='row'>
                <div className='col'>
                    <h2>Weather in {data.city}, {data.country}</h2>
                </div>
            </div>
            <div className='row'>
                <div className='col-4'>
                    <div className='row'>
                        <div className='col'>
                            <img src={data.iconURL} width="50px" height="50px" alt=""/>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col'>
                            <p>{data.iconDesc}</p>
                        </div>
                    </div>
                </div>
                <div className='col align-self-center'>
                    <h3>{data.temp} °C</h3>
                </div>
            </div>
            <div className='row'>
                <div className='col'>
                    <h4>{dateTime}</h4>
                </div>
            </div>
            <div className='row'>
                <div className='col'>
                    <table className="table table-striped">
                        <tbody>
                            <tr>
                                <th scope="row">Wind</th>
                                <td>{data.wind} m/s</td>
                            </tr>
                            <tr>
                                <th scope="row">Cloudiness</th>
                                <td>{data.clouds}%</td>
                            </tr>
                            <tr>
                                <th scope="row">Pressure</th>
                                <td>{data.pressure} hpa</td>
                            </tr>
                            <tr>
                                <th scope="row">Humidity</th>
                                <td>{data.humidity}%</td>
                            </tr>
                            <tr>
                                <th scope="row">Sunrise</th>
                                <td>{sunrise}</td>
                            </tr>
                            <tr>
                                <th scope="row">Sunset</th>
                                <td>{sunset}</td>
                            </tr>
                            <tr>
                                <th scope="row">Geo Coords</th>
                                <td>[{data.lat}, {data.lon}]</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};
export default Mobile;
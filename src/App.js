import './App.css';
import Mobile from './components/Mobile';

function App() {
  return (
    <div className='App'>
      <header className='App-header'>
        <div className='container'>
          <Mobile />
        </div>
      </header>
    </div>
  );
}

export default App;
